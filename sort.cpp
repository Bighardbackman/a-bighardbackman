#include <iostream>
#include <vector>

using std::cin, std::cout, std::cerr, std::endl, std::vector;

/**
\fn Sort
\brief Sort data 
\param [in] The "array" A[0..n-1] containing items to be sorted 
\returns The total number of key comparisons made
*/
unsigned long int Sort(auto& A )
{
  unsigned long int count = 0;

 vector<int> B;
    int q,r,p, x, y;

       B.push_back(0);
       B.push_back(A.size()-1);

     while (B.size() != 0)
       {
         r = B[B.size() -1];
         q = B[B.size()-2];
         B.pop_back();
         B.pop_back();

        p = A[q+(r-q)/2];
        x = r, y = q;

        while (true)
        {

          while(count++ && A[y] < p)
              y++;
          

          while(count++ && A[x] > p)
              x--;

          if(y >= x) 
              break;

          std::swap(A[x], A[y]);
	  count++;
          y++;
          x--;
        }


       if (q < x)
       {
          B.push_back(q);
          B.push_back(x);
        }
      if (x+1 < r)
        {
          B.push_back(x+1);
          B.push_back(r);
        }
      }//end while


 
  return count;
}//End of Sort 

int main()
{
  vector<int> inputs;
  int input;

   cerr<<"Welcome to \"Sort\". We first need some input data."<<endl;
   cerr<<"To end input type Ctrl+D (followed by Enter)"<<endl<<endl;

   
 
    while(cin>>input)//read an unknown number of inputs from keyboard
    {
       inputs.push_back(input);
    }

   cerr<<endl<<"|  Number of inputs | Number of comparisons |"<<endl;
   cerr<<"|\t"<<inputs.size();
   cout<<"| "<<Sort(inputs);
    
   cerr<<"\t|"<<endl<<endl<<"Program finished."<<endl<<endl;

    return 0;
}
